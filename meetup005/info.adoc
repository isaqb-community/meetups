== 5. Ruhrgebiet-Community-Event in Dortmund!
* *Datum*: 21. Feb. 2024, 18:00 - 20:00 Uhr, anschließend diskutieren mit Pizza 😋
* *Ort*: Signal-Iduna, Joseph-Scherer-Straße 3, 44139 Dortmund

=== Themen

[cols="100,1", width="100%"]
|===
| Thema | Stimmen

| Architektur im Team erstellen oder doch lieber _den Architekten_; Architektur-Arbeit im/vom Team | 9
| Wie "statisch" sollte Architektur sein? | 7
| Anbindung Legacy-System während Migration | 6
| Ab wann darf man sich Architekt nennen? | 4
| Wie entstehen Hypes? | 4
| Technologie-Innovationen in Unternehmen | 3
| "Prozesskontrolle" in Event-getriebenen Architekturen | 2
| Geh mir weg mit Spring :) | 2
| Sollte man in starren Unternehmen immer nur "Langlebiges" und "Bewährtes" nutzen? | 1
| Wenn ein Event-basierendes Datenhaltungssystem (mit S3) ein operatives System (BODS) mit Daten versorgt, ist das ein Antipattern? Muss man ein System operativ oder dispositiv auseinander halten?| 0
|===

=== Eindrücke

[grid="none", frame="none"]
|===
|image:meetup005/IMG_5441.jpg[] | image:meetup005/1708544076045.jpg[]
|===
